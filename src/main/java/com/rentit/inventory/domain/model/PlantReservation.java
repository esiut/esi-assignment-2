package com.rentit.inventory.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservation {
    @Id @GeneratedValue
    Long id;

    @ManyToOne (cascade = {CascadeType.ALL})
    PlantInventoryItem plant;

    @Embedded
    BusinessPeriod schedule;
}
