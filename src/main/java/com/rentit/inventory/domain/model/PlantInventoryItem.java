package com.rentit.inventory.domain.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PlantInventoryItem {
    @Id @GeneratedValue
    Long id;

    public static PlantInventoryItem of(String serialNumber, PlantInventoryEntry plantInfo){
        PlantInventoryItem plantInventoryItem = new PlantInventoryItem();
        plantInventoryItem.serialNumber = serialNumber;
        plantInventoryItem.plantInfo = plantInfo;

        return plantInventoryItem;
    }

    public static PlantInventoryItem of(Long id, String serialNumber,EquipmentCondition equipmentCondition, PlantInventoryEntry plantInfo){
        PlantInventoryItem plantInventoryItem = new PlantInventoryItem();
        plantInventoryItem.id = id;
        plantInventoryItem.equipmentCondition = equipmentCondition;
        plantInventoryItem.serialNumber = serialNumber;
        plantInventoryItem.plantInfo = plantInfo;

        return plantInventoryItem;
    }

    String serialNumber;
    @Enumerated(EnumType.STRING)
    EquipmentCondition equipmentCondition;

    @ManyToOne
    PlantInventoryEntry plantInfo;
}
