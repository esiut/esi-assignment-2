package com.rentit.sales.application.services;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.EquipmentCondition;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.validatiors.BPValidator;
import com.rentit.sales.application.validatiors.POValidator;
import com.rentit.sales.application.validatiors.PlantInvEntryValidator;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.util.List;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    public PurchaseOrderDTO findPurchaseOrder(Long id) {
        PurchaseOrder po = purchaseOrderRepository.findOne(id);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO partialPODTO) throws PlantNotFoundException {
        PlantInventoryEntry plant = plantInventoryEntryRepository.findOne(partialPODTO.getPlant().get_id());
        BusinessPeriod rentalPeriod = BusinessPeriod.of(partialPODTO.getRentalPeriod().getStartDate(), partialPODTO.getRentalPeriod().getEndDate());
        BigDecimal total = partialPODTO.getPlant().getPrice();

        PurchaseOrder po = PurchaseOrder.of(
                plant,
                rentalPeriod,
                total,
                POStatus.PENDING);

        po = purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public List<PurchaseOrderDTO> findPendingOrders(POStatus status) {
        List<PurchaseOrder> po = purchaseOrderRepository.findByStatus(status);

        return purchaseOrderAssembler.toResources(po);
    }

    public PurchaseOrderDTO preallocatePurchaseOrder(PurchaseOrderDTO poDto) {
        BusinessPeriod rentalPeriod = BusinessPeriod.of(poDto.getRentalPeriod().getStartDate(), poDto.getRentalPeriod().getEndDate());
        BigDecimal total = poDto.getPlant().getPrice();

        RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();

        List<PlantInventoryEntry> plants = inventoryRepository.findAvailablePlants(poDto.getPlant().getName(),
                rentalPeriod.getStartDate(), rentalPeriod.getEndDate());

        if (plants.isEmpty()) {
            System.out.println("EROOOR");
        }

        PurchaseOrder po = PurchaseOrder.of(
                plants.get(0),
                rentalPeriod,
                total,
                POStatus.PREALLOCATED);

        po = purchaseOrderRepository.save(po);

        PlantInventoryItem plantInventoryItem = PlantInventoryItem.of(generator.generate(3).toUpperCase(), plants.get(0));

        PlantReservation plantReservation = new PlantReservation();
        plantReservation.setPlant(plantInventoryItem);
        plantReservation.setSchedule(rentalPeriod);

        plantReservationRepository.save(plantReservation);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO confirmPurchaseOrder(PurchaseOrderDTO poDto) {
        PlantInventoryEntry plantInventoryEntry = plantInventoryEntryRepository.findOne(poDto.getPlant().get_id());
        BusinessPeriod businessPeriod = BusinessPeriod.of(poDto.getRentalPeriod().getStartDate(), poDto.getRentalPeriod().getEndDate());
        BigDecimal total = poDto.getPlant().getPrice();
        PurchaseOrder po = PurchaseOrder.of(
                plantInventoryEntry,
                businessPeriod,
                total,
                POStatus.OPEN);

        return purchaseOrderAssembler.toResource(purchaseOrderRepository.save(po));
    }

    public PurchaseOrderDTO rejectPurchaseOrder(PurchaseOrderDTO poDto) {
        PlantInventoryEntry plantInventoryEntry = plantInventoryEntryRepository.findOne(poDto.getPlant().get_id());
        BusinessPeriod businessPeriod = BusinessPeriod.of(poDto.getRentalPeriod().getStartDate(), poDto.getRentalPeriod().getEndDate());
        BigDecimal total = poDto.getPlant().getPrice();
        PurchaseOrder po = PurchaseOrder.of(
                plantInventoryEntry,
                businessPeriod,
                total,
                POStatus.REJECTED);

        return purchaseOrderAssembler.toResource(purchaseOrderRepository.save(po));
    }
}
