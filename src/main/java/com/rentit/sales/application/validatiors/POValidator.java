package com.rentit.sales.application.validatiors;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

/**
 * Created by Evgeniy on 22.03.2018.
 */
public class POValidator implements Validator {

    private final Validator plantInvEntryValidator;
    private final Validator businessPeriodValidator;


    public POValidator(Validator plantInvEntryValidator, Validator businessPeriodValidator) {
        if (plantInvEntryValidator == null || businessPeriodValidator == null)
            throw new IllegalArgumentException("The supplied [Validator] is " +
                    "required and must not be null.");

        if (!plantInvEntryValidator.supports(PlantInventoryEntryDTO.class))
            throw new IllegalArgumentException("The supplied [Validator] must " +
                    "support the validation of [PlantInventoryEntryDTO] instances.");

        if (!businessPeriodValidator.supports(BusinessPeriodDTO.class))
            throw new IllegalArgumentException("The supplied [Validator] must " +
                    "support the validation of [BusinessPeriodDTO] instances.");

        this.plantInvEntryValidator = plantInvEntryValidator;
        this.businessPeriodValidator = businessPeriodValidator;
    }

    @Override
    public boolean supports(Class clazz) {
        return PurchaseOrderDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "total", "total.empty", "Field total empty.");
        ValidationUtils.rejectIfEmpty(errors, "status", "status.empty", "Field status empty.");

        PurchaseOrderDTO po = (PurchaseOrderDTO) o;

        if (po.getTotal() != null)
            if (po.getTotal().compareTo(BigDecimal.ZERO) == -1)
                errors.rejectValue("total", "total.subZero");

        if (po.getStatus() != null)
            if (!checkStatus(po.getStatus()))
                errors.rejectValue("status", "status.unknownStatus");

        errors.pushNestedPath("plant");
        ValidationUtils.invokeValidator(this.plantInvEntryValidator, po.getPlant(), errors);
        errors.popNestedPath();

        errors.pushNestedPath("rentalPeriod");
        ValidationUtils.invokeValidator(this.businessPeriodValidator, po.getRentalPeriod(), errors);
        errors.popNestedPath();

    }

    private boolean checkStatus(POStatus status){
        POStatus[] allStatuses = POStatus.values();
        for (POStatus item:allStatuses)
            if (item.equals(status))
                return true;
        return false;
    }
}
