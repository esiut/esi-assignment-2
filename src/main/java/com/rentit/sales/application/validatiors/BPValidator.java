package com.rentit.sales.application.validatiors;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;

/**
 * Created by Evgeniy on 22.03.2018.
 */
public class BPValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return BusinessPeriodDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "startDate", "startDate.empty", "Field startDate empty.");
        ValidationUtils.rejectIfEmpty(errors, "endDate", "endDate.empty", "Field endDate empty.");

        BusinessPeriodDTO bp = (BusinessPeriodDTO) o;

        if(bp.getStartDate() != null && bp.getEndDate() != null) {
            if (!bp.getStartDate().isBefore(bp.getEndDate()))
                errors.rejectValue("endDate", "endDate.tooearly");

            if (!bp.getStartDate().isAfter(LocalDate.now()))
                errors.rejectValue("startDate", "startDate.tooearly");
        }

    }
}
