package com.rentit.sales.application.validatiors;

import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Evgeniy on 22.03.2018.
 */
public class PlantInvEntryValidator implements Validator {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Override
    public boolean supports(Class clazz) {
        return PlantInventoryEntryDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty", "Field name empty.");

        PlantInventoryEntryDTO pie = (PlantInventoryEntryDTO) o;

        if (pie.getPrice() != null)
            if (pie.getPrice().compareTo(BigDecimal.ZERO) == -1)
              errors.rejectValue("price", "price.subZero");

//        if (pie.getName() != null){
//            List<PlantInventoryEntry> plant = plantInventoryEntryRepository.findByNameContaining(pie.getName());
//            if (plant != null)
//                errors.rejectValue("plant", "plant.noPlantInDatabase");
//        }
    }
}
