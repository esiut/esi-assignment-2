package com.rentit.sales.rest;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.application.validatiors.BPValidator;
import com.rentit.sales.application.validatiors.POValidator;
import com.rentit.sales.application.validatiors.PlantInvEntryValidator;
import com.rentit.sales.domain.model.POStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/sales")
public class SalesRestController {

    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    @ResponseStatus(HttpStatus.OK)
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailable(plantName, startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPurchaseOrder(id);
    }

    @PutMapping("/orders")
    public ResponseEntity createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws URISyntaxException, PlantNotFoundException {

        //Validator
        DataBinder binder = new DataBinder(partialPODTO);
        binder.addValidators(new POValidator(new PlantInvEntryValidator(), new BPValidator()));
        binder.validate();
        if (!binder.getBindingResult().hasErrors()) {
            PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(partialPODTO);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newlyCreatePODTO.getId().getHref()));

            return new ResponseEntity<PurchaseOrderDTO>(newlyCreatePODTO, headers, HttpStatus.CREATED);
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(binder.getBindingResult().getAllErrors());
        }
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
        // Code To handle Exception
    }


    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> findPurchaseOrderByStatus(@RequestParam(name = "status") POStatus status){

        //Handle empty status
        return salesService.findPendingOrders(status);
    }




    @PutMapping("/preallocate")
    public ResponseEntity preallocatePurchaseOrder(@RequestBody PurchaseOrderDTO poDto) throws URISyntaxException {

        //Validator
        DataBinder binder = new DataBinder(poDto);
        binder.addValidators(new POValidator(new PlantInvEntryValidator(), new BPValidator()));
        binder.validate();
        if (!binder.getBindingResult().hasErrors()) {
            PurchaseOrderDTO newPo = salesService.preallocatePurchaseOrder(poDto);

            //Handle exc


            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newPo.getId().getHref()));

            return new ResponseEntity<>(newPo, headers, HttpStatus.CREATED);
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(binder.getBindingResult().getAllErrors());
        }

    }


    @PutMapping("/accept")
    public ResponseEntity confirmPreallocation(@RequestBody PurchaseOrderDTO poDto) throws URISyntaxException {
        //Validator
        DataBinder binder = new DataBinder(poDto);
        binder.addValidators(new POValidator(new PlantInvEntryValidator(), new BPValidator()));
        binder.validate();
        if (!binder.getBindingResult().hasErrors()) {
            PurchaseOrderDTO newPo = salesService.confirmPurchaseOrder(poDto);

            //Handle exc

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newPo.getId().getHref()));

            return new ResponseEntity<>(newPo, headers, HttpStatus.OK);
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(binder.getBindingResult().getAllErrors());
        }
    }

    @PutMapping("/reject")
    public ResponseEntity rejectPreallocation(@RequestBody PurchaseOrderDTO poDto) throws URISyntaxException {
        //Validator
        DataBinder binder = new DataBinder(poDto);
        binder.addValidators(new POValidator(new PlantInvEntryValidator(), new BPValidator()));
        binder.validate();
        if (!binder.getBindingResult().hasErrors()) {
            PurchaseOrderDTO newPo = salesService.confirmPurchaseOrder(poDto);

            //Handle exc

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newPo.getId().getHref()));

            return new ResponseEntity<>(newPo, headers, HttpStatus.OK);
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(binder.getBindingResult().getAllErrors());
        }
    }


}
