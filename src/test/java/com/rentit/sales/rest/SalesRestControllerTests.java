package com.rentit.sales.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.services.SalesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@DirtiesContext
public class SalesRestControllerTests {
    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    SalesService salesService;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testGetAllPlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        assertThat(plants.size()).isEqualTo(3);

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));

        System.out.println(result.getResponse().getContentAsString());

        MvcResult result2 = mockMvc.perform(put("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();

        // Testing methods, can delete later

        MvcResult result22 = mockMvc.perform(put("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();

        PurchaseOrderDTO po = mapper.readValue(result2.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {});

        System.out.println(po.get_id());

        MvcResult result3 = mockMvc.perform(get("/api/sales/orders/"+po.get_id())).andReturn();

        System.out.println(result3.getResponse().getContentAsString());

        MvcResult result4 = mockMvc.perform(get("/api/sales/orders?status=PENDING")).andReturn();

        System.out.println(result4.getResponse().getContentAsString());

        MvcResult result5 = mockMvc.perform(put("/api/sales/preallocate").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();

        System.out.println(result5.getResponse().getContentAsString());


        MvcResult result6 = mockMvc.perform(put("/api/sales/accept").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        System.out.println(result6.getResponse().getContentAsString());
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testRecentlyCreatedPO() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        PurchaseOrderDTO order = new PurchaseOrderDTO();

        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2018,04,15), LocalDate.of(2018,04,20)));

        LocalDate currentDate = LocalDate.now();
        LocalDate end = order.getRentalPeriod().getEndDate();
        LocalDate start = order.getRentalPeriod().getStartDate();

        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(order);

        assertThat(end.isAfter(currentDate) && start.isAfter(currentDate)).isTrue();
        assertThat(start.isBefore(end)).isTrue();
        assertThat(start.equals(null) || end.equals(null)).isFalse();

        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/"+newlyCreatePODTO.get_id())).andReturn();
        System.out.println(result2.getResponse().getContentAsString());
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testValidIdentifierPO() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));

        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(order);

        assertThat(newlyCreatePODTO.get_id().equals(null)).isFalse();

        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/"+newlyCreatePODTO.get_id())).andReturn();
        System.out.println(result2.getResponse().getContentAsString());
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testValidPlantReservation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        PurchaseOrderDTO order = new PurchaseOrderDTO();

        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2018,04,15), LocalDate.of(2018,04,20)));

        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(order);
        PurchaseOrderDTO po = salesService.preallocatePurchaseOrder(newlyCreatePODTO);

        BusinessPeriodDTO orderPeriod = po.getRentalPeriod();
        //check date

        assertThat(po.getPlant()).isNotNull();

    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testValidTotalCost() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));

        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(order);

        assertThat(newlyCreatePODTO.getTotal().compareTo(BigDecimal.ZERO) < 0).isFalse();
        assertThat(newlyCreatePODTO.getTotal().equals(null)).isFalse();
    }

}
